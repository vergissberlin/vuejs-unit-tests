import Vue from 'vue'
import FancyHeading from '@/components/FancyHeading.vue'

function mountComponentWithProps (Component, propsData) {
    const Constructor = Vue.extend(Component)
    const vm = new Constructor({
        propsData
    }).$mount()

    return vm.$el
}

describe('FancyHeading.vue', () => {
    it('should be the correct color', () => {
        const headingData = mountComponentWithProps(FancyHeading, { color: 'blue' })
        const styleData = headingData.style.getPropertyValue('color')

        console.log(styleData)

        expect(styleData).toEqual('blue')
    })

    it('should have the correct title', () => {
        const headingData = mountComponentWithProps(FancyHeading, { title: 'Hello, Vue!' })
        const titleData = headingData.textContent

        expect(titleData).toEqual('Hello, Vue!')
    })

    it('should have the correct title even with HTML in it', () => {
        const headingData = mountComponentWithProps(FancyHeading, { title: 'Hello, <b>Vue</b>!' })
        const titleData = headingData.textContent

        expect(titleData).toEqual('Hello, <b>Vue</b>!')
    })
})

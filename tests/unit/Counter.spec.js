import { mount } from '@vue/test-utils'
import Counter from '@/components/Counter.vue'
import Vue from 'vue'

describe('Counter', () => {
    // Now mount the component and you have the wrapper
    const wrapper = mount(Counter)

    it('renders the correct markup', () => {
        expect(wrapper.html()).toContain('<span class="count">0</span>')
    })

    // it's also easy to check for the existence of elements
    it('has a button', () => {
        expect(wrapper.contains('button')).toBe(true)
    })

    it('button click should increment the count', () => {
        expect(wrapper.vm.count).toBe(0)
        const button = wrapper.find('button')
        button.trigger('click')
        expect(wrapper.vm.count).toBe(1)
    })

    it('button click should increment the count text', async () => {
        expect(wrapper.text()).toContain('1 Increment')
        const button = wrapper.find('button')
        button.trigger('click')
        await Vue.nextTick()
        expect(wrapper.text()).toContain('2')
    })
})

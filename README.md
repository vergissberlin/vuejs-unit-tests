# vuejs-unit-tests

[![pipeline status](https://gitlab.com/vergissberlin/vuejs-unit-tests/badges/master/pipeline.svg)](https://gitlab.com/vergissberlin/vuejs-unit-tests/-/commits/master)
[![coverage report](https://gitlab.com/vergissberlin/vuejs-unit-tests/badges/master/coverage.svg)](https://gitlab.com/vergissberlin/vuejs-unit-tests/-/commits/master)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
